This directory contains Symbols and Footprints that are modified versions of
original KiCAD Symbols and Footprints.

The original KiCAD library license applies.

Please check the KiCAD homepage and repository for original authors and updated
materials.

https://www.kicad.org/libraries/download/
