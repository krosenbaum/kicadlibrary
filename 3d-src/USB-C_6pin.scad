// USB-C for OpenSCAD
// (c) Konrad Rosenbaum, 2021
// License: CC-BY-SA
//

include <lib/USB-C_common.scad>;

rotate([0,0,180])
translate([-$width/2,0,0])
union(){
    body(6.8, 1.2);
    //mechanical feet
    translate([0,0,-1])
     color($silver)
     cube([$thickness,0.8,3]);
    translate([0,3.8,-1])
     color($silver)
     cube([$thickness,0.8,3]);
    translate([$width-$thickness,0,-1])
     color($silver)
     cube([$thickness,0.8,3]);
    translate([$width-$thickness,3.8,-1])
     color($silver)
     cube([$thickness,0.8,3]);
    //pins
    translate([$width/2+0.5,0,0])pin(0.7, 1.2);
    translate([$width/2-0.5,0,0])pin(0.7, 1.2);
    translate([$width/2+1.52,0,0])pin(0.76, 1.2);
    translate([$width/2-1.52,0,0])pin(0.76, 1.2);
    translate([$width/2+2.75,0,0])pin(0.8, 1.2);
    translate([$width/2-2.75,0,0])pin(0.8, 1.2);
}

//END OF FILE
