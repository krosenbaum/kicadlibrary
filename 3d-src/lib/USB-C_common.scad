// USB-C for OpenSCAD
// (c) Konrad Rosenbaum, 2021
// License: CC-BY-SA
//

//basics
$fn=20;

$black="#222";
$silver="#99A";
$white="#fff";
$gold="#AA7";

//calculations

$thickness = 0.3;
$width = 8.94;
$height = 3.2;

//3D

module oval(width,height,depth,ocolor=$silver)
{
    union(){
        //central body horizontally
        translate([0,0,height/3])
        color(ocolor)
        cube([width,depth,height/3]);
        //central body vertically
        translate([height/3,0,0])
        color(ocolor)
        cube([width-height*2/3,depth,height]);
        //rounded corners
        translate([height/3,depth/2,height/3])
        rotate([90,0,0])
        color(ocolor)
        cylinder(h=depth,r=height/3,center=true);
        translate([height/3,depth/2,height*2/3])
        rotate([90,0,0])
        color(ocolor)
        cylinder(h=depth,r=height/3,center=true);
        translate([width-height/3,depth/2,height/3])
        rotate([90,0,0])
        color(ocolor)
        cylinder(h=depth,r=height/3,center=true);
        translate([width-height/3,depth/2,height*2/3])
        rotate([90,0,0])
        color(ocolor)
        cylinder(h=depth,r=height/3,center=true);
    }
}

module body(depth,recess=0){
    union(){
        //outer body
        difference(){
            oval($width, 3.2,  depth);
            translate([$thickness,-1,$thickness])
             oval($width-2*$thickness, 2.6, depth+2);
            translate([$thickness,-0.1,-0.1])
             color($silver)
             cube([$width-2*$thickness, recess+0.1,2]);
        }
        //plastic parts
        translate([$thickness,recess,$thickness])
         oval($width-2*$thickness, 2.6, depth-4.5, $black);
        translate([1.12,depth-4.6,1.25])
         color($black)
         cube([6.7,4.5,0.7]);
    }
}

module pin(width, depth)
{
    union(){
        translate([-width/2,0,0])color($gold)cube([width,depth,0.1]);
        translate([-width/2,depth-0.1,0])color($gold)cube([width,0.1,0.8]);
    }
}

//END OF FILE
