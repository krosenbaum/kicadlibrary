translate([-8.05,-3.5])
union(){
  //main body
  color("grey")cube([16.1,7,7.5]);
  //row A
  translate([3.7,2.2,-3])color("grey")cube([.5,.4,4]);
  translate([7.7,2.2,-3])color("grey")cube([.5,.4,4]);
  translate([9.7,2.2,-3])color("grey")cube([.5,.4,4]);
  translate([11.7,2.2,-3])color("grey")cube([.5,.4,4]);
  //row B
  translate([3.7,4.4,-3])color("grey")cube([.5,.4,4]);
  translate([7.7,4.4,-3])color("grey")cube([.5,.4,4]);
  translate([9.7,4.4,-3])color("grey")cube([.5,.4,4]);
  translate([11.7,4.4,-3])color("grey")cube([.5,.4,4]);
  //shield
  translate([0,0,-3])color("grey")cube([1,.4,4]);
  translate([15.1,0,-3])color("grey")cube([1,.4,4]);
  translate([0,6.6,-3])color("grey")cube([1,.4,4]);
  translate([15.1,6.6,-3])color("grey")cube([1,.4,4]);
  //lever
  translate([3.5,-3.5,4.5])color("#111")cube([4,5,2.9]);
}