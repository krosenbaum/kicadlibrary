$fn=20;
translate([-7.5,-7,0])
difference(){
    //basic outline (15x10x20)
    cube([15, 10, 20]);
    //TO-220 space (2mm fins, 7mm deep)
    translate([1,-1,-1])cube([13,8,22]);
    //mini fin slots (1x1mm)
    translate([1,9,-1])cube([1,2,22]);
    translate([3,9,-1])cube([1,2,22]);
    translate([5,9,-1])cube([1,2,22]);
    translate([7,9,-1])cube([1,2,22]);
    translate([9,9,-1])cube([1,2,22]);
    translate([11,9,-1])cube([1,2,22]);
    translate([13,9,-1])cube([1,2,22]);
    //screw hole (M3, at h=16.5)
    translate([7.5,6,16.5])
    rotate([90,0,0])
    cylinder(10,r=1.5,center=true);
}
