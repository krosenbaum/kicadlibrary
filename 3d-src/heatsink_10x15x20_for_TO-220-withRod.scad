$fn=20;
translate([-7.5,-3.5,0])
union(){
    difference(){
        //basic outline (15x10x20)
        cube([15, 10, 20]);
        //TO-220 space (1mm fins, 3.5mm deep)
        translate([1,-.5,-1])cube([13,4,22]);
        //deep fin slots (w=1.5 d=5)
        translate([1,5,-1])cube([1.5,6,22]);
        translate([3.5,5,-1])cube([1.5,6,22]);
        translate([10,5,-1])cube([1.5,6,22]);
        translate([12.5,5,-1])cube([1.5,6,22]);
        //center slot (w=3, d=2.5)
        translate([6,7.5,-1])cube([3,6,22]);
        //screw hole (M3, at h=16.5)
        translate([7.5,6,16.5])
        rotate([90,0,0])
        cylinder(10,r=1.5,center=true);
    }
//bottom rod (3mm down, 1mm wide, at .5mm sunk into center slot)
translate([7.5,6,0])
//color("green")
cylinder(6,r=0.6,center=true);
}