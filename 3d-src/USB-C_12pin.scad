// USB-C for OpenSCAD
// (c) Konrad Rosenbaum, 2021
// License: CC-BY-SA
//

include <lib/USB-C_common.scad>;

rotate([0,0,180])
translate([-$width/2,0,0])
union(){
    body(7.3, .3);
    //mechanical feet
    translate([0,0,0])
     color($silver)
     cube([$thickness,1.05,2]);
    translate([-1.2,0,0])
     color($silver)
     cube([1.2,1.05,$thickness]);

    translate([$width-$thickness,0,0])
     color($silver)
     cube([$thickness,1.05,2]);
    translate([$width-$thickness,0,0])
     color($silver)
     cube([1.2,1.05,$thickness]);

    translate([0,4.2,0])
     color($silver)
     cube([$thickness,0.8,2]);
    translate([-1.2,4.2,0])
     color($silver)
     cube([1.2,0.8,$thickness]);

    translate([$width-$thickness,4.2,0])
     color($silver)
     cube([$thickness,.8,2]);
    translate([$width-$thickness,4.2,0])
     color($silver)
     cube([1.2,.8,$thickness]);

    //position pins
    translate([$width/2+2.89,1.05,-.45])
     color($gold)
     cylinder(h=1.1, r=0.25, center=true);
    translate([$width/2-2.89,1.05,-.45])
     color($gold)
     cylinder(h=1.1, r=0.25, center=true);

    //pins
    translate([$width/2+0.25,-.6,0])pin(0.3, .8);
    translate([$width/2+0.75,-.6,0])pin(0.3, .8);
    translate([$width/2+1.25,-.6,0])pin(0.3, .8);
    translate([$width/2+1.75,-.6,0])pin(0.3, .8);

    translate([$width/2-0.25,-.6,0])pin(0.3, .8);
    translate([$width/2-0.75,-.6,0])pin(0.3, .8);
    translate([$width/2-1.25,-.6,0])pin(0.3, .8);
    translate([$width/2-1.75,-.6,0])pin(0.3, .8);

    translate([$width/2+2.45,-.6,0])pin(0.6, .8);
    translate([$width/2+3.25,-.6,0])pin(0.6, .8);

    translate([$width/2-2.45,-.6,0])pin(0.6, .8);
    translate([$width/2-3.25,-.6,0])pin(0.6, .8);
}

//END OF FILE
