#!/bin/bash
cd $(dirname $0)

####################################
# Config

#where to find Chipper (use absolute paths!)
CHIPPER_DIR=`pwd`/chipper/build5
CHIPPER=$CHIPPER_DIR/bin/chipper-cmd

#targets
VPREFIX=WIZZARD
PREFIXDIR=$(pwd)

#FreeCAD / OpenSCAD
FREECAD=freecad
OPENSCAD=openscad

####################################
# Build

#cleanup
rm -rf 3dmodels Symbols Footprints || true
mkdir 3dmodels Symbols Footprints

#init fixed files
cd 3dmodels
#ln -s ../3d_fixed/* .
cd ../Symbols
ln -s ../Symbols_fixed/* .
cd ../Footprints
ln -s ../Footprints_fixed/* .
cd ..

#chipper run
$CHIPPER -generate -envprefix=$VPREFIX -root=$PREFIXDIR $(find ./Chips -name '*.chip')


#auto-conversion of 3d-src OpenSCAD models
PYSCR=$CHIPPER_DIR/Templates/generator/3d-openscad-fcad.py

cd 3d-src
for f in *.scad ; do
    $OPENSCAD -o ${f%.scad}.csg $f
    sed 's:{{tempDir}}/model:'${f%.scad}':' <$PYSCR >conv.py
    $FREECAD conv.py
done

mv *.step *.wrl ../3dmodels/
rm -rf *.csg conv.py __pycache__ || true

echo
echo
echo DONE.
