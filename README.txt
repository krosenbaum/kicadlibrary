Wizzard.Industries Libraries
==============================

This Repository contains KiCAD libraries, associated templates and generators.
Most of them are for KiCAD version 6, the few that are in KiCAD 5 format will be converted soon.


Copyright
----------

The contents of this Repository, if not explicitly listed below with a
different copyright, are (c) Konrad Rosenbaum, 2021
protected under the KiCAD Library License (see Kicad_License.txt).

Footprints/modified.pretty contains footprints derived from original KiCad footprints.
Symbols/modified contains symbols derived from original KiCad symbols.
In both cases: originally provided by the KiCAD team and modified by Konrad Rosenbaum.
Protected under the KiCAD Library License.

Symbols/Logo and Footprints/Logo.pretty contain the Wizzard.Industries Logo,
which is for use by its author and on original Wizzard.Industries branded PCBs
only. Please do not use it on your own PCBs and remove it or replace it with
the "based on a WI design" version of the logo if you alter a PCB design.
(c) Konrad Rosenbaum, 2021 - all rights reserved.

Chipper contains the sources for the symbol/footprint/3dmodel generator app.
It is protected under the GNU GPL v.3 or at your option any newer.
See the Chipper directory for details.


Directories
------------

Footprints: KiCad footprints of various types.

  .../modified.pretty - modified KiCad footprints
  .../Logo.pretty - Wizzard.Industries Logo for original PCBs
  .../HeatSinks.pretty - Heat Sink footprints

Symbols: KiCad symbols of various types.

  .../modified - modified KiCad symbols
  .../Logo - Wizzard.Industries Logo for original PCBs
  .../* - WI symbols...

3d-src: OpenSCAD or FreeCAD sources for 3D models that were not generated using
  Chipper

3dmodels: STEP or VRML exports of 3D models

Chipper: the chip symbol/footprint generator, see inside this directory for
    details

Chips: contains chip description files that can be translated into symbols,
    footprints and 3D models using the Chipper application.


Using the Library
------------------

Create the following environment variables (KiCad main Window,
menu "Preferences" -> "Configure Paths"; upper table "Environment Variables":

WIZZARD_3DMODEL_DIR = ***/3dmodels
WIZZARD_SYMBOL_DIR = ***/Symbols
WIZZARD_FOOTPRINT_DIR = ***/Footprints

(replace "***" with the path to this repository)

Always use the base directory name as variable value, so that all symbols or
footprints inside are visible.

In projects in which you are using the libraries add them to the project
specific libraries by using these variables. In version 6 KiCAD will usually
automatically detect if a path is relative to one of those variables and use
the variable automatically.

Usually you will need to configure:

Symbols:
    menu "Preferences" -> "Manage Symbol Libraries..."
    tab "Project Specific Libraries"
    you will need to add each library file that you plan to use, the "Nickname"
    should be the base of the file name, e.g.:
    Symbols/Modified/XConnector.kicad_sym -> Nickname = "XConnector"

Footprints:
    menu "Preferences" -> "Manage Footprint Libraries..."
    tab "Project Specific Libraries"
    add each *.pretty directory that you plan to use, the "Nickname" should be
    the base name of that directory, e.g.
    Footprints/Modified.pretty -> Nickname = "Modified"
    -> if you use a different nickname then KiCAD will not automatically find
    the correct footprint for symbols and you have to assign it manually

3D Models:
    menu "Preferences" -> "Configure Paths", lower table "3D Search Paths"
    add a line with "${WIZZARD_3DMODEL_DIR}" as path, the Alias and Description
    columns can be set to any value.
